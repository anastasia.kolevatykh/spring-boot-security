package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.TaskNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Task;

import java.util.List;

public interface ITaskService {
    @NotNull Task getTaskEntity(@Nullable TaskDTO taskDTO) throws Exception;

    @NotNull TaskDTO getTaskDTO(@Nullable Task task) throws Exception;

    @NotNull List<TaskDTO> getListTaskDTO(@Nullable List<Task> tasks) throws Exception;

    @NotNull List<Task> findAll() throws Exception;

    @NotNull List<Task> findAllByUserId(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findTasksWithProjectId(@Nullable String userId) throws UserNotFoundException;

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id)
            throws Exception, UserNotFoundException, EmptyInputException;

    @NotNull List<Task> findOneByName(@Nullable String userId, @Nullable String name)
            throws Exception, UserNotFoundException, EmptyInputException;

    void persist(@Nullable Task task) throws TaskNotFoundException;

    void merge(@Nullable Task task) throws TaskNotFoundException;

    void remove(@Nullable String userId, @Nullable String id)
                    throws Exception, UserNotFoundException, EmptyInputException;

    void removeAllByUserId(@Nullable String userId)
                            throws Exception, UserNotFoundException;

    void removeAll();

    void removeTasksWithProjectId(@Nullable String userId)
                                    throws Exception, UserNotFoundException;

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId)
                                            throws Exception, UserNotFoundException, EmptyInputException;

    @NotNull List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId)
            throws Exception, UserNotFoundException, EmptyInputException;

    @NotNull List<Task> findTasksWithoutProject(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByCreateDate(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStartDateAsc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStartDateDesc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStartDateAsc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStartDateDesc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByFinishDateAsc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByFinishDateDesc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByFinishDateAsc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByFinishDateDesc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStatusAsc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    List<Task> findAllSortedByStatusDesc(@Nullable String userId)
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStatusAsc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllSortedByStatusDesc()
            throws Exception, UserNotFoundException;

    @NotNull List<Task> findAllBySearch(@Nullable String userId, @Nullable String search)
            throws Exception;

    @NotNull List<Task> findAllBySearch(@Nullable String search) throws Exception;
}
