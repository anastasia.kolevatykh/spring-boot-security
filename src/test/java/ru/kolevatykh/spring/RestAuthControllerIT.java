package ru.kolevatykh.spring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import ru.kolevatykh.spring.api.client.AuthResourceClient;
import ru.kolevatykh.spring.api.controller.IRestLoginController;
import ru.kolevatykh.spring.service.PropertyService;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = PropertyService.class)
@TestPropertySource
public class RestAuthControllerIT {

    @Autowired
    private PropertyService property;

    @Test
    public void testAuth() {
        IRestLoginController authResourceClient
                = AuthResourceClient.getInstance(property.getServer());
        String authStatus = authResourceClient.login("admin1", "admin1");
        System.out.println(authStatus);
        assertTrue(authStatus.contains("200"));
        authResourceClient.logout();
    }
}
